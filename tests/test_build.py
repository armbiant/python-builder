"""
Test the build process and execute the resulting binary to ensure correct output
"""
# Import python libs
import mock
import pytest
import subprocess


def test_build(hub, pb_bin, pb_conf):
    with mock.patch("sys.argv", ["pytest_arg0", "build", "--config", pb_conf]):
        hub.tiamat.init.cli()

    cp = subprocess.run(
        pb_bin, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    assert b"pb works!" in cp.stdout
    assert b"hashids works!" in cp.stdout
    assert b"bodger" in cp.stdout


@pytest.mark.expensive_test
def test_pkgbuild(hub, pb_bin, pb_conf):
    with mock.patch(
        "sys.argv", ["pytest_arg0", "build", "--config", pb_conf, "--pkg-tgt", "arch"]
    ):
        hub.tiamat.init.cli()

    cp = subprocess.run(
        pb_bin, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    assert b"pb works!" in cp.stdout
    assert b"hashids works!" in cp.stdout
    assert b"bodger" in cp.stdout
